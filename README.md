# Module: Docker Build

Gitlab-CI Module for Build and Publish Docker Images.  

Template file for gitlab-ci pipeline: [api.yaml](templates/api.yaml)  

## Module Jobs

Template job name              | Description
-----------------              | -----------
`.docker_build.build_rc`       | Build rc docker image and upload to artifactory.  
`.docker_build.publish_tag`    | Publication rc image for tags.  
`.docker_build.publish_branch` | Publication rc image for branches.  
`.docker_build.publish_latest` | Publication rc as latest.  
`.docker_build.cleanup_rc`     | Clearing temporary rc image from artifactory.  

## How to use

Include in gitlab-ci pipeline module template:

```yaml
include:
  - project: 'devops2213409/ci-cd-pipeline/ci-cd-modules/module_docker-build'
    ref: v0.0.1  # replace with the latest release
    file: 'templates/api.yaml'
```

Review and override parameters in `.docker_build.variables` if necessary.  
for example:

```yaml
.docker_build.variables:
  variables:
    DOCKER_FILE: "build/Dockerfile_test
```

Use the jobs from module template in your pipeline.  
for example:

```yaml
build_docker:
  extends: .docker_build.build_rc

publish_docker:
  extends: .docker_build.publish_branch
  only:
    - master

cleanup_docker:
  extends: .docker_build.cleanup_rc
  stage: Cleanup
```
